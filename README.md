# eslint-config-baguette

**This repository is not used anymore. See the [Baguette toolkit](https://gitlab.com/baguetteswap/baguette-toolkit) instead**

Baguette Eslint config with:

- Airbnb config
- Typescript
- Prettier

## Usage

```
npx install-peerdeps --dev @baguetteswap-libs/eslint-config-baguette
```

Add `"extends": "@baguetteswap-libs/eslint-config-baguette"` to your eslint config file.
